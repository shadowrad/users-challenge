# How to run
## docker
docker-compose up 

## virtualenv
* pip install -r requirements.txt
* ./manage.py migrate


# Using it 
## Settings 
if you want to test the system using the the Schema 
* "rest_framework.authentication.SessionAuthentication" uncomment this line in settings (the test is not going to work)  

if you want to test the system using a token uncomment this line in settings
* "rest_framework.authentication.TokenAuthentication"  

