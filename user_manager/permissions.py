# the others methods can only be call if the user is staff or superuser
from rest_framework.permissions import BasePermission


class StaffOrSuperPermissions(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser or request.user.is_staff
