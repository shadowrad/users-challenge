import re

from django.contrib.auth.models import User, Group
from rest_framework import serializers


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['name']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name']


class FullUserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True)
    password = serializers.SerializerMethodField()

    class Meta:
        model = User
        exclude = ['user_permissions']

    def get_password(self, instance):
        return '*************'


class CreateUserSerializer(serializers.ModelSerializer):
    groups = serializers.ListField(child=serializers.CharField(), write_only=True)
    repeat_password = serializers.CharField(default='*************')

    class Meta:
        model = User
        exclude = ['user_permissions']

    def create(self, validated_data):
        groups_data = validated_data.pop('groups')
        __ = validated_data.pop('repeat_password')
        user = User.objects.create(**validated_data)
        for group_data in groups_data:
            group, __ = Group.objects.get_or_create(name=group_data)
            user.groups.add(group)
        return user

    def update(self, instance, validated_data):
        groups_data = validated_data.pop('groups')
        groups = Group.objects.filter(name__in=groups_data)
        instance.groups.set(groups)
        return instance

    def validate(self, attrs):
        if attrs['password'] != attrs['repeat_password']:
            raise serializers.ValidationError({'password': "Make sure both passwords match"}, code=400)

        #  Password must include lowercase and upercase letters, digits and symbols. At least 8 chars
        if len(self.initial_data['password']) < 8:
            raise serializers.ValidationError({'password': "Password must have at least 8 chars"}, code=400)

        has_upper = bool(re.search('[A-Z]', attrs['password']))
        if not has_upper:
            raise serializers.ValidationError({'password': "Password must include uppercase letter"}, code=400)

        has_lower = bool(re.search('[a-z]', attrs['password']))
        if not has_lower:
            raise serializers.ValidationError({'password': "Password must include lowercase letter"}, code=400)

        has_number = bool(re.search('[0-9]', attrs['password']))

        if not has_number:
            raise serializers.ValidationError({'password': "Password must include digits"}, code=400)

        has_symbol = not attrs['password'].isalnum()
        if not has_symbol:
            raise serializers.ValidationError({'password': "Password must include symbols"}, code=400)

        return attrs
