from django.contrib.auth.models import User
from django.test import Client
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase


class UserTests(APITestCase):
    def setUp(self) -> None:
        self.auth_url = '/api-token-auth/'
        self.user_url = '/user/'
        password = 'admin123'
        user_admin = User.objects.create_superuser('admin_test', 'admin@admin.com', password)
        token, created = Token.objects.get_or_create(user=user_admin)
        self.client = Client(HTTP_AUTHORIZATION='Token ' + token.key)

    def test_create_user(self):
        """
        Ensure we can create a new account object.
        """
        data = {
            "username": "johndoe",
            "first_name": "John",
            "last_name": "Doe",
            "email": "johndoe@ine.test",
            "password": "SuperSecure@Passwd1",
            "repeat_password": "SuperSecure@Passwd1",
            "groups": ["sales", "support"]
        }

        response = self.client.post(self.user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        user = User.objects.get(pk=response.json()['id'])
        self.assertEqual(user.username, 'johndoe')

    def test_create_bad_repeat_pass(self):
        """
        Ensure we check repeat_password and password.
        """
        data = {
            "username": "johndoe",
            "first_name": "John",
            "last_name": "Doe",
            "email": "johndoe@ine.test",
            "password": "SuperSecure@Passwd",
            "repeat_password": "SuperSsswd",
            "groups": ["sales", "support"]
        }

        response = self.client.post(self.user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['password'][0], "Make sure both passwords match")

    def test_create_bad_long_pass(self):
        """
        Ensure we check Password must have at least 8 chars"
        """
        data = {
            "username": "johndoe",
            "first_name": "John",
            "last_name": "Doe",
            "email": "johndoe@ine.test",
            "password": "Sup",
            "repeat_password": "Sup",
            "groups": ["sales", "support"]
        }

        response = self.client.post(self.user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['password'][0], "Password must have at least 8 chars")

    def test_create_bad_pass_symbols(self):
        """
        Ensure we check Password must include symbols
        """
        data = {
            "username": "johndoe",
            "first_name": "John",
            "last_name": "Doe",
            "email": "johndoe@ine.test",
            "password": "Superpassword1",
            "repeat_password": "Superpassword1",
            "groups": ["sales", "support"]
        }

        response = self.client.post(self.user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['password'][0], "Password must include symbols")

    def test_create_bad_pass_uppercase(self):
        """
        Ensure we check Password must include uppercase
        """
        data = {
            "username": "johndoe",
            "first_name": "John",
            "last_name": "Doe",
            "email": "johndoe@ine.test",
            "password": "a12345678",
            "repeat_password": "a12345678",
            "groups": ["sales", "support"]
        }

        response = self.client.post(self.user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['password'][0], "Password must include uppercase letter")

    def test_create_bad_pass_lower(self):
        """
        Ensure we check Password must include lower
        """
        data = {
            "username": "johndoe",
            "first_name": "John",
            "last_name": "Doe",
            "email": "johndoe@ine.test",
            "password": "A12345678",
            "repeat_password": "A12345678",
            "groups": ["sales", "support"]
        }

        response = self.client.post(self.user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['password'][0], "Password must include lowercase letter")

    def test_create_bad_email(self):
        """
        Ensure we check email
        """
        data = {
            "username": "johndoe",
            "first_name": "John",
            "last_name": "Doe",
            "email": "jjjsdjsdx",
            "password": "A12345678",
            "repeat_password": "A12345678",
            "groups": ["sales", "support"]
        }

        response = self.client.post(self.user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json()['email'][0], "Enter a valid email address.")

    def test_delete_staff_as_super(self):
        """
        Ensure we check delete staff as superuser
        """
        user_to_del = User.objects.create_user('user_to_del', 'user@mail.com', 'admin123', is_staff=True)
        del_user_id = str(user_to_del.id) + '/'
        response = self.client.delete(self.user_url + del_user_id, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_as_staff(self):
        """
        Ensure we check delete user as staff
        """
        staff_user = User.objects.create_user('staff_user', 'staff_user@mail.com', 'admin123', is_staff=True)
        user_to_del = User.objects.create_user('user_to_del', 'user@mail.com', 'admin123')
        token, created = Token.objects.get_or_create(user=staff_user)
        staff_client = Client(HTTP_AUTHORIZATION='Token ' + token.key)
        del_user_id = str(user_to_del.id) + '/'
        response = staff_client.delete(self.user_url + del_user_id, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_staff_as_staff(self):
        """
        Ensure we check delete staff as staff
        """
        staff_user = User.objects.create_user('staff_user', 'staff_user@mail.com', 'admin123', is_staff=True)
        staff_to_del = User.objects.create_user('staff_user_to_del', 'user@mail.com', 'admin123', is_staff=True)
        token, created = Token.objects.get_or_create(user=staff_user)
        staff_client = Client(HTTP_AUTHORIZATION='Token ' + token.key)
        del_user_id = str(staff_to_del.id) + '/'
        response = staff_client.delete(self.user_url + del_user_id, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_user_as_self(self):
        """
        Ensure we check retreave user as that user
        """
        staff_user = User.objects.create_user('staff_user', 'staff_user@mail.com', 'admin123')
        token, created = Token.objects.get_or_create(user=staff_user)
        staff_client = Client(HTTP_AUTHORIZATION='Token ' + token.key)
        del_user_id = str(staff_user.id) + '/'
        response = staff_client.get(self.user_url + del_user_id, format='json')
        self.assertContains(response, 'email')
        self.assertContains(response, 'groups')

    def test_get_as_staff(self):
        """
        Ensure we check retreave user as that staff
        """
        staff_user = User.objects.create_user('staff_user', 'staff_user@mail.com', 'admin123', is_staff=True)
        staff_to_get = User.objects.create_user('staff_user_to_get', 'user@mail.com', 'admin123')
        token, created = Token.objects.get_or_create(user=staff_user)
        staff_client = Client(HTTP_AUTHORIZATION='Token ' + token.key)
        del_user_id = str(staff_to_get.id) + '/'
        response = staff_client.get(self.user_url + del_user_id, format='json')
        self.assertContains(response, 'email')
        self.assertContains(response, 'groups')

    def test_get_as_other(self):
        """
        Ensure we check retreave user as other user
        """
        staff_user = User.objects.create_user('staff_user', 'staff_user@mail.com', 'admin123')
        staff_to_get = User.objects.create_user('staff_user_to_get', 'user@mail.com', 'admin123')
        token, created = Token.objects.get_or_create(user=staff_user)
        staff_client = Client(HTTP_AUTHORIZATION='Token ' + token.key)
        del_user_id = str(staff_to_get.id) + '/'
        response = staff_client.get(self.user_url + del_user_id, format='json')
        self.assertNotContains(response, 'email')
        self.assertNotContains(response, 'groups')
