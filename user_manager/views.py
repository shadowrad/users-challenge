from django.shortcuts import render

# Create your views here.
# ViewSets define the view behavior.
from rest_framework import viewsets, status
from django.contrib.auth.models import User, Group
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly, IsAuthenticated
from user_manager.permissions import StaffOrSuperPermissions

from user_manager.serializers import UserSerializer, GroupSerializer, FullUserSerializer, CreateUserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if 'pk' in self.kwargs:
            user = self.request.user
            if str(user.pk) == self.kwargs['pk'] or user.is_staff:
                return FullUserSerializer
        return UserSerializer

    def create(self, request, *args, **kwargs):
        serializer = CreateUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        serializer = FullUserSerializer(instance=serializer.instance)
        return Response(serializer.data, status=201)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = CreateUserSerializer(instance=instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=200)

    def destroy(self, request, *args, **kwargs):
        user = self.get_object()
        if user.is_staff and not request.user.is_superuser:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        self.perform_destroy(user)
        return Response(status=status.HTTP_204_NO_CONTENT)


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [StaffOrSuperPermissions]
